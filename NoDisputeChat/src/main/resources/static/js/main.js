var usernamePage = document.querySelector('#username-page');
var chatPage = document.querySelector('#chat-page');
var usernameForm = document.querySelector('#usernameForm');
var messageForm = document.querySelector('#messageForm');
var messageInput = document.querySelector('#message');
var messageArea = document.querySelector('#messageArea');
var connectingElement = document.querySelector('.connecting');
var disputeForm = document.querySelector('#disputeForm');
var choiceInput = document.querySelector('#choice');

var stompClient = null;
var username = null;
var choice = null;

var colors = [
    '#2196F3', '#32c787', '#00BCD4', '#ff5652',
    '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
];

function connect(event) {
    username = document.querySelector('#name').value.trim();

    if(username) {
        usernamePage.classList.add('hidden');
        chatPage.classList.remove('hidden');

        var socket = new SockJS('/ws');
        stompClient = Stomp.over(socket);

        stompClient.connect({}, onConnected, onError);
    }
    event.preventDefault();
}


function onConnected() {
    // Subscribe to the Public Topic
    stompClient.subscribe('/topic/public', onMessageReceived);

    // Tell your username to the server
    stompClient.send("/app/chat.addUser",
        {},
        JSON.stringify({sender: username, type: 'JOIN'})
    )

    connectingElement.classList.add('hidden');
}

function itemLizard(event){
choice = document.querySelector('.green').getAttribute("data-choice");

 if(choice && stompClient){
         var textMessage = {
             sender: username,
             choice: choice,
             type: 'DISPUTE'
         };

         stompClient.send("/app/chat.disputePrevention", {}, JSON.stringify(textMessage));

     }
 }

function itemPaper(event){
choice = document.querySelector('.blue').getAttribute("data-choice");

if(choice && stompClient){
        var textMessage = {
            sender: username,
            choice: choice,
            type: 'DISPUTE'
        };

        stompClient.send("/app/chat.disputePrevention", {}, JSON.stringify(textMessage));
        choice = '';
    }
}

function itemSpock(event){
choice = document.querySelector('.lightblue').getAttribute("data-choice");

if(choice && stompClient){
        var textMessage = {
            sender: username,
            choice: choice,
            type: 'DISPUTE'
        };

        stompClient.send("/app/chat.disputePrevention", {}, JSON.stringify(textMessage));

    }
}

function itemScissors(event){
choice = document.querySelector('.orange').getAttribute("data-choice");

if(choice && stompClient){
        var textMessage = {
            sender: username,
            choice: choice,
            type: 'DISPUTE'
        };

        stompClient.send("/app/chat.disputePrevention", {}, JSON.stringify(textMessage));
        choice = '';
    }
}

function itemRock(event){
choice = document.querySelector('.red').getAttribute("data-choice");

if(choice && stompClient){
        var textMessage = {
            sender: username,
            choice: choice,
            type: 'DISPUTE'
        };

        stompClient.send("/app/chat.disputePrevention", {}, JSON.stringify(textMessage));
        choice = '';
    }
}

function onError(error) {
    connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
    connectingElement.style.color = 'red';
}

function sendMessage(event) {
    var messageContent = messageInput.value.trim();

    if(messageContent && stompClient) {
        var textMessage = {
            sender: username,
            content: messageInput.value,
            type: 'CHAT'
        };

        stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(textMessage));
        messageInput.value = '';
    }
    event.preventDefault();
}

function onMessageReceived(payload) {
    var message = JSON.parse(payload.body);

    var messageElement = document.createElement('li');

    if(message.type === 'JOIN') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' joined!';
    } else if (message.type === 'LEAVE') {
        messageElement.classList.add('event-message');
        message.content = message.sender + ' left!';
    }else if (message.type === 'DISPUTE'){
        messageElement.classList.add('event-message');
       message.content = message.sender + ' choosed ' + message.choice + ' result is --> ' + message.result;
    } else {
        messageElement.classList.add('chat-message');

        var avatarElement = document.createElement('i');
        var avatarText = document.createTextNode(message.sender[0]);
        avatarElement.appendChild(avatarText);
        avatarElement.style['background-color'] = getAvatarColor(message.sender);

        messageElement.appendChild(avatarElement);

        var usernameElement = document.createElement('span');
        var usernameText = document.createTextNode(message.sender);
        usernameElement.appendChild(usernameText);
        messageElement.appendChild(usernameElement);
    }

    var textElement = document.createElement('p');
    var messageText = document.createTextNode(message.content);
    textElement.appendChild(messageText);

    messageElement.appendChild(textElement);

    messageArea.appendChild(messageElement);
    messageArea.scrollTop = messageArea.scrollHeight;
}


function getAvatarColor(messageSender) {
    var hash = 0;
    for (var i = 0; i < messageSender.length; i++) {
        hash = 40 * hash + messageSender.charCodeAt(i);
    }

    var index = Math.abs(hash % colors.length);
    return colors[index];
}


usernameForm.addEventListener('submit', connect, true)
messageForm.addEventListener('submit', sendMessage, true)
