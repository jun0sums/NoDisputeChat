package com.juno.NoDisputeChat.NoDisputeChat.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;



@Configuration
@EnableWebSocketMessageBroker
// Використовується для активації ВебСокету сервера
// Інтерфейс визначає методи налаштування обробки повідомлень за допомогою простих протоколів обміну повідомленнями
public class WebSockConfig implements WebSocketMessageBrokerConfigurer {
    // В першому методі реєструємо кінцеву точку websocket,
    // яку клієнти будуть використовувати для подключения до серверу websocket
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws").withSockJS();
        // withSockJS() використовується для включения резервних опцій для браузерів,
        // які не підтримують websocket.
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // registry - Реєстр для налаштування параметрів брокера повідомлень
        registry.setApplicationDestinationPrefixes("/app");
        registry.enableSimpleBroker("/topic");
        // Підключення брокера повідомлень і налаштування префікса для фільтрації напрямку
    }

}