package com.juno.NoDisputeChat.NoDisputeChat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoDisputeChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoDisputeChatApplication.class, args);
	}
}
