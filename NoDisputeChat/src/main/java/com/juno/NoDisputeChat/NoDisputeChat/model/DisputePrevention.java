package com.juno.NoDisputeChat.NoDisputeChat.model;

import java.util.Random;

public class DisputePrevention {
    private String [] choices = {"ROCK", "PAPER", "SCISSORS", "LIZARD", "SPOCK"};
    private String randomItem(){
        int idx = new Random().nextInt(choices.length);
        return (choices[idx]);
    }

    public String toDispute(String itemChoice) {
        String item = randomItem();
        if (item.equals(itemChoice)) {
            return "DRAW";
        } else {
            switch (itemChoice) {
                case "ROCK" :
                    if (item.equals("SCISSORS"))
                        return("WIN");
                    else if (item.equals("LIZARD"))
                        return("WIN");
                    else
                        return("LOST");
                case "SCISSORS":
                    if (item.equals("PAPER"))
                        return("WIN");
                    else if (item.equals("LIZARD"))
                        return("WIN");
                    else
                        return("LOST");
                case "PAPER":
                    if (item.equals("ROCK"))
                        return("WIN");
                    else if (item.equals("SPOCK"))
                        return("WIN");
                    else
                        return("LOST");
                case "LIZARD":
                    if (item.equals("PAPER"))
                        return("WIN");
                    else if (item.equals("SPOCK"))
                        return("WIN");
                    else
                        return("LOST");
                case "SPOCK":
                    if (item.equals("ROCK"))
                        return("WIN");
                    else if (item.equals("SCISSORS"))
                        return("WIN");
                    else
                        return("LOST");
            }
        }
        return "";
    }
}
