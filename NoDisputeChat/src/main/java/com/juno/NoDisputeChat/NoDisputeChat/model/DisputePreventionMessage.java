package com.juno.NoDisputeChat.NoDisputeChat.model;



import static com.juno.NoDisputeChat.NoDisputeChat.model.TextMessage.*;

public class DisputePreventionMessage {

    private String sender;
    private String result;
    private String content;
    private String choice;
    private MessageType type;

    DisputePrevention disputePrevention = new DisputePrevention();

    public MessageType getType() { return type; }
    public String getChoice() { return choice; }
    public String getSender() { return sender; }
    public String getContent() { return content; }
    public String getResult() { return disputePrevention.toDispute(choice); }
    public void setChoice(String choice) { this.choice = choice; }
    public void setSender(String sender) { this.sender = sender; }


}