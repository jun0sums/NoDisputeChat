package com.juno.NoDisputeChat.NoDisputeChat.controller;

import com.juno.NoDisputeChat.NoDisputeChat.model.DisputePreventionMessage;
import com.juno.NoDisputeChat.NoDisputeChat.model.TextMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import org.springframework.stereotype.Controller;

@Controller
public class ChatControl {

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")

    public TextMessage sendMessage(@Payload TextMessage textMessage){ return textMessage; }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")

    public TextMessage addUser(@Payload TextMessage textMessage,
                               SimpMessageHeaderAccessor headerAccessor){
        //добавити користувача з даним ім'ям в сесію
        headerAccessor.getSessionAttributes().put("username", textMessage.getSender());
        return textMessage;
    }


    @MessageMapping("/chat.disputePrevention")
    @SendTo("/topic/public")
    public DisputePreventionMessage disputePrevention(@Payload DisputePreventionMessage textMessage)  {


        return textMessage;
    }
}
