package com.juno.NoDisputeChat.NoDisputeChat.controller;

import com.juno.NoDisputeChat.NoDisputeChat.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.*;

@Component
public class WebListener {
    private static final Logger logger = LoggerFactory.getLogger(WebListener.class);

    @Autowired
    private SimpMessageSendingOperations messaging;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("New web socket connection");
    }

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event){
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap((event.getMessage()));
         String username =(String)headerAccessor.getSessionAttributes().get("username");
        if(username != null){
            logger.info("User Disconnected " + username);
        }
            TextMessage textMessage = new TextMessage();
            textMessage.setType(TextMessage.MessageType.LEAVE);
            textMessage.setSender(username);
            messaging.convertAndSend("/topic/public", textMessage);
        }
}
